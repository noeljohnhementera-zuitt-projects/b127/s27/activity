let http = require('http');

let users = [
{
	"firstName": "Mary Jane",
	"lastName": "Dela Cruz",
	"mobileNo": "09123456789",
	"email": "mjdelacruz@mail.com",
	"password": 123
},
{
	"firstName": "John",
	"lastName": "Doe",
	"mobileNo": "09123456789",
	"email": "jdoe@mail.com",
	"password": 123
}
]

http.createServer(function(request, response){
	if (request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "application/json"});
		response.write(JSON.stringify(users));
		response.end();
	}

	if (request.url == "/register" && request.method == "POST"){
		let requestBody = "";

		request.on("data", function(data){
			requestBody += data
		})

		request.on("end", function(){
			console.log(typeof requestBody)
			requestBody = JSON.parse(requestBody);

			let newlyRegisteredUser = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}

			users.push(newlyRegisteredUser)
			console.log(users);

			response.writeHead(200, {"Content-Type": "application/json"})
			response.write(JSON.stringify(newlyRegisteredUser))
			response.end();
		})
	}
}).listen(4000);

console.log("Server running at localhost:4000");